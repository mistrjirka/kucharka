<?php
require("configureauth.php");
function my_json_encode($data) {
    if( json_encode($data) === false ) {
        throw new Exception( json_last_error() );
    }else{
        return json_encode($data);
    }
}
if ($_GET['name']) {
    header('Content-Type: text/html; charset=utf-8');

    $name = filter_var('%'.$_GET["name"].'%', FILTER_SANITIZE_STRING);
   
    if ($stmt = $con->prepare('SELECT * FROM receptynove WHERE nazev Like ?')) {
        // Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
        $stmt->bind_param('s', $name);
        $stmt->execute();
        // Store the result so we can check if the account exists in the database.
        //$stmt->store_result();
        $result = $stmt->get_result();
        $toReturn = "[]";
        while ($row = $result->fetch_assoc()) {
            if($row["rating"] === "" || $row["rating"] === " "){
                $toReturn = "[]";
            }else{
                try {
                    my_json_encode($row["rating"]);
                    $toReturn = $row["rating"];
                }
                catch(Exception $e ) {
                    $toReturn = "[]";
                }
            }
        }

        $stmt->close();
        echo $toReturn;
    }else{
    }
    
} else {
    die("internal error: name not set correctly, please contact admin");
}
?>