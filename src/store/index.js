import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    seznam: []
  },
  mutations: {
    addToSeznam: (state, payload) => {
      return state.seznam.push(payload);
    },
    
    removeFromSeznam: (state, payload) => {
      for (var i = 0; i < state.seznam.length; i++) {
        if (state.seznam[i] === payload) {
          return state.seznam.splice(i, 1);
        }
      }
    },
    removeSeznam: (state)=>{
      return [];
    }
  },
  actions: {
  },
  modules: {
  }
})
