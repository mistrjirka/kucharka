import Vue from 'vue';
import App from './App.vue';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import router from './router'
Vue.config.productionTip = false
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
import VueClipboard from 'vue-clipboard2'

import BootstrapVue from "bootstrap-vue";
Vue.use(VueClipboard);

Vue.use(BootstrapVue, VueAxios, axios);

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')

