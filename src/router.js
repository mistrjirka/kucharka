import Vue from 'vue'
import Router from 'vue-router'
import Recepty from "./recepty.vue"

Vue.use(Router)


export default new Router({
  routes: [

    // dynamic segments start with a colon
    {
      path: '/:id', component: Recepty
    }
  ]
})
